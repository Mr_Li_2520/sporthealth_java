package com.lixin.sportplay.controller;

import com.alibaba.fastjson.JSON;
import com.lixin.sportplay.bean.QueryInfo;
import com.lixin.sportplay.bean.User;
import com.lixin.sportplay.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping("/allUser")
    public String getUserList(QueryInfo queryInfo) {
        int numbers = userDao.getUserCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();
        List<User> users = userDao.getAllUser("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", users);
        System.out.println("总条数：" + numbers);
        String users_json = JSON.toJSONString(res);
        return users_json;
    }

    @RequestMapping("/allUser1")
    public String getUserList1(QueryInfo queryInfo1) {
        int numbers = userDao.getUserCounts1("%" + queryInfo1.getQuery() + "%");
        int pageStart = (queryInfo1.getPageNum() - 1) * queryInfo1.getPageSize();
        List<User> users = userDao.getAllUser1("%" + queryInfo1.getQuery() + "%", pageStart, queryInfo1.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", users);
        System.out.println("总条数：" + numbers);
        String users_json = JSON.toJSONString(res);
        return users_json;
    }

    @RequestMapping("/userState")
    public String updateUserState(@RequestParam("id") Integer id,
                                  @RequestParam("state") Boolean state) {
        int i = userDao.updateState(id, state);
        System.out.println("用户编号:" + id);
        System.out.println("用户状态:" + state);
        String str = i > 0 ? "success" : "error";
        return str;
    }

    @RequestMapping("/addUser")
    public String addUser(@RequestBody User user) {
        System.out.println(user);
//        user.setRole("普通用户");
//        user.setState(true);
        int i = userDao.addUser(user);
        String str = i > 0 ? "success" : "error";
        return str;
    }

    @RequestMapping("/getUpdate")
    public String getUpdateUser(int id) {
        System.out.println("编号:" + id);
        User updateUser = userDao.getUpdateUser(id);
        String users_json = JSON.toJSONString(updateUser);
        return users_json;
    }

    @RequestMapping("/getNum")
    public String getNumUser(int id) {
        System.out.println("编号:" + id);
        User updateNum = userDao.getUpdateUser(id);
        String users_json = JSON.toJSONString(updateNum);
        return users_json;
    }

    @RequestMapping("/editUser")
    public String editUser(@RequestBody User user) {
        System.out.println(user);
        int i = userDao.editUser(user);
        String str = i > 0 ? "success" : "error";
        return str;
    }

    @RequestMapping("/deleteUser")
    public String deleteUser(int id) {
        System.out.println(id);
        int i = userDao.deleteUser(id);
        String str = i > 0 ? "success" : "error";
        return str;
    }
}
