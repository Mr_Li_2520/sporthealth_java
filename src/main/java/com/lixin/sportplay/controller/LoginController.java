package com.lixin.sportplay.controller;

import com.lixin.sportplay.bean.User;
import com.lixin.sportplay.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    UserDao userDao;

    @RequestMapping("/login")
    public String login(@RequestBody User user) {
//        String flag = "error";
//        User us = userDao.getUserByMassage(user.getUsername(), user.getPassword());
//        System.out.println("user:" + us);
//        HashMap<String, Object> res = new HashMap<>();
//        if (us != null) {
//            flag = "ok";
//        }
//        res.put("flag",flag);
//        res.put("user",user);
//        String res_json = JSON.toJSONString(res);
//        return res_json;
        System.out.println("User : " + user);
        String str = "error";
        int count = userDao.getUserByMassage(user.getUsername(), user.getPassword());
        if (count > 0) {
            str = "ok";
        }
        return str;
    }
}
