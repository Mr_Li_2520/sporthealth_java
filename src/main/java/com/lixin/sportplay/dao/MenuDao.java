package com.lixin.sportplay.dao;

import com.lixin.sportplay.bean.MainMenu;

import java.util.List;

public interface MenuDao {
    public List<MainMenu> getMainMenus();
}
