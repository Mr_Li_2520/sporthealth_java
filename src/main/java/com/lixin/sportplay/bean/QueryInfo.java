package com.lixin.sportplay.bean;

public class QueryInfo {
    private String query; // 查询信息 username
    private int query1; // 查询信息 phone
    private int pageNum = 1;// 当前页
    private int pageSize = 10;// 每页最大数

    public QueryInfo() {
    }

    public QueryInfo(String query, int query1, int pageNum, int pageSize) {
        this.query = query;
        this.query1 = query1;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getQuery1() {
        return query1;
    }

    public void setQuery1(int query1) {
        this.query1 = query1;
    }
    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "QueryInfo{" +
                "query='" + query + '\'' +
                ", query1=" + query1 +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                '}';
    }
}
